Create Generator SeqEmployee;
/*******************************************************************************
 * Tables
 * ------
 * Extracted at 18/09/2021 11:25:05 AM
 ******************************************************************************/

Create Table Employee 
(
  EmployeeID  Integer Not Null,
  FirstName   Varchar(25) Not Null,
  LastName    Varchar(25) Not Null,
  Photo       Blob Sub_Type 0 Segment Size 80,
  Updno       Integer Default 0 Not Null,
  Constraint Pk_Employee Primary Key (EmployeeID)
);
