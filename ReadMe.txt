Please restore a database named DB-Concurrency.fdb in the Database folder with the help of the backup file DB-Concurrency.bak.

Register the database in the aliases.config of Firebird like this:
DBConcurrency=[YourFolder]\DB-Concurrency\Database\DB-Concurrency.fdb

The FireDAC connection uses the alias instead of a direct file location.