unit DMain;

interface

uses
  System.Classes,
  Data.DB,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Phys.IBWrapper,
  FireDAC.Phys.IBBase,
  FireDAC.Moni.Base,
  FireDAC.Moni.Custom;

type
  TUpdateMode = (umWhereAll, umWhereKey, umWhereChanged, umPKAndUpNo);

  TOnMonitorOutputEvent = procedure(const AMessage: string) of object;

  TdmoMain = class(TDataModule)
    conEmployee: TFDConnection;
    qryEmployee: TFDQuery;
    qryEmployeeEMPLOYEEID: TIntegerField;
    qryEmployeeFIRSTNAME: TStringField;
    qryEmployeeLASTNAME: TStringField;
    qryEmployeePHOTO: TGraphicField;
    qryEmployeeUPDNO: TIntegerField;
    FDMoniCustomClientLink1: TFDMoniCustomClientLink;
    procedure FDMoniCustomClientLink1Output(ASender: TFDMoniClientLinkBase;
                                            const AClassName, AObjName, AMessage: string);
    procedure qryEmployeeNewRecord(DataSet: TDataSet);
    procedure qryEmployeeBeforePost(DataSet: TDataSet);
  private
    FOnMonitorOutput: TOnMonitorOutputEvent;
    procedure SetOnMonitorOutput(const Value: TOnMonitorOutputEvent);
    { Private declarations }
  public
    { Public declarations }
    procedure OpenEmployee;
    procedure SetUpdateMode(const UpdateMode: TUpdateMode);
    procedure LoadPhoto(const FileName: string);
    property OnMonitorOutput: TOnMonitorOutputEvent read FOnMonitorOutput
      write SetOnMonitorOutput;
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TdmoMain }

procedure TdmoMain.FDMoniCustomClientLink1Output(ASender: TFDMoniClientLinkBase;
                                                 const AClassName, AObjName, AMessage: string);
begin
  if Assigned(FOnMonitorOutput) then
    FOnMonitorOutput(AMessage);
end;

procedure TdmoMain.LoadPhoto(const FileName: string);
begin
  if qryEmployee.State = dsBrowse then
    qryEmployee.Edit;
  qryEmployeePHOTO.LoadFromFile(FileName);
end;

procedure TdmoMain.OpenEmployee;
begin
  FDMoniCustomClientLink1.Tracing := True;
  qryEmployee.Open;
end;

procedure TdmoMain.qryEmployeeBeforePost(DataSet: TDataSet);
begin
  if (qryEmployee.State = dsEdit) and
     (pfInKey in qryEmployeeUPDNO.ProviderFlags) then
    qryEmployeeUPDNO.AsInteger := qryEmployeeUPDNO.OldValue + 1;
end;

procedure TdmoMain.qryEmployeeNewRecord(DataSet: TDataSet);
begin
  qryEmployeeUPDNO.AsInteger := 0;
end;

procedure TdmoMain.SetOnMonitorOutput(const Value: TOnMonitorOutputEvent);
begin
  FOnMonitorOutput := Value;
end;

procedure TdmoMain.SetUpdateMode(const UpdateMode: TUpdateMode);
begin
  qryEmployeeUPDNO.ProviderFlags := [pfInUpdate, pfInWhere];
  case UpdateMode of
    umWhereAll:
      qryEmployee.UpdateOptions.UpdateMode := upWhereAll;
    umWhereKey:
      qryEmployee.UpdateOptions.UpdateMode := upWhereKeyOnly;
    umWhereChanged:
      qryEmployee.UpdateOptions.UpdateMode := upWhereChanged;
    umPKAndUpNo:
      begin
        qryEmployee.UpdateOptions.UpdateMode := upWhereKeyOnly;
        qryEmployeeUPDNO.ProviderFlags := [pfInUpdate, pfInWhere, pfInKey];
      end;
  end;
end;

end.
