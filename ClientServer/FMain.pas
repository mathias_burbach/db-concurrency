unit FMain;

interface

uses
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Data.DB,
  Vcl.DBCtrls,
  Vcl.StdCtrls,
  Vcl.Mask,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.ExtCtrls,
  Vcl.Menus;

type
  TfrmMain = class(TForm)
    pnlEmployee: TPanel;
    pnlLog: TPanel;
    pnlLogHeader: TPanel;
    meoLog: TMemo;
    pnlEmployeeDetails: TPanel;
    grdEmployee: TDBGrid;
    pnlEmployeeOptions: TPanel;
    navEmployee: TDBNavigator;
    dscEmployee: TDataSource;
    edtFirstName: TDBEdit;
    lblFirstName: TLabel;
    Label1: TLabel;
    edtLastName: TDBEdit;
    imgPhoto: TDBImage;
    cbxUpdateMode: TComboBox;
    Label2: TLabel;
    dlgSelectBitmap: TOpenDialog;
    pmePhoto: TPopupMenu;
    menChangePhoto: TMenuItem;
    procedure cbxUpdateModeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure menChangePhotoClick(Sender: TObject);
  private
    { Private declarations }
    procedure DoMonitorOutput(const AMessage: string);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  System.SysUtils,
  DMain;

const
  cCRLF = CHR(13) + CHR(10);

procedure TfrmMain.cbxUpdateModeChange(Sender: TObject);
var
  UpdateMode: TUpdateMode;
begin
  UpdateMode := TUpdateMode(cbxUpdateMode.ItemIndex);
  dmoMain.SetUpdateMode(UpdateMode);
end;

procedure TfrmMain.DoMonitorOutput(const AMessage: string);
begin
  meoLog.Lines.Add(StringReplace(AMessage, cCRLF, ' ', [rfReplaceAll]));
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dmoMain := TdmoMain.Create(Self);
  dmoMain.OnMonitorOutput := DoMonitorOutput;
  dmoMain.OpenEmployee;
end;

procedure TfrmMain.menChangePhotoClick(Sender: TObject);
begin
  if dlgSelectBitmap.Execute then
    dmoMain.LoadPhoto(dlgSelectBitmap.FileName);
end;

end.
