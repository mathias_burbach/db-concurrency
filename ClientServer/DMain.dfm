object dmoMain: TdmoMain
  OldCreateOrder = False
  Height = 280
  Width = 522
  object conEmployee: TFDConnection
    Params.Strings = (
      'Database=DBConcurrency'
      'User_Name=sysdba'
      'Protocol=TCPIP'
      'Server=localhost'
      'Password=masterkey'
      'OSAuthent=No'
      'MonitorBy=Custom'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 64
  end
  object qryEmployee: TFDQuery
    BeforePost = qryEmployeeBeforePost
    OnNewRecord = qryEmployeeNewRecord
    Connection = conEmployee
    UpdateOptions.AssignedValues = [uvGeneratorName]
    UpdateOptions.GeneratorName = 'SEQEMPLOYEE'
    UpdateOptions.KeyFields = 'EMPLOYEEID'
    SQL.Strings = (
      'Select *'
      'From Employee'
      'Order By EmployeeID')
    Left = 192
    Top = 64
    object qryEmployeeEMPLOYEEID: TIntegerField
      DisplayLabel = 'ID'
      DisplayWidth = 5
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object qryEmployeeFIRSTNAME: TStringField
      DisplayLabel = 'FirstName'
      DisplayWidth = 15
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
      Size = 25
    end
    object qryEmployeeLASTNAME: TStringField
      DisplayLabel = 'LastName'
      DisplayWidth = 15
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
      Size = 25
    end
    object qryEmployeePHOTO: TGraphicField
      FieldName = 'PHOTO'
      Origin = 'PHOTO'
      Visible = False
      BlobType = ftGraphic
    end
    object qryEmployeeUPDNO: TIntegerField
      DisplayLabel = 'UpdNo'
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
      Required = True
    end
  end
  object FDMoniCustomClientLink1: TFDMoniCustomClientLink
    EventKinds = [ekCmdExecute]
    OnOutput = FDMoniCustomClientLink1Output
    Left = 144
    Top = 168
  end
end
