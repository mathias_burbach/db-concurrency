object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'DB Concurreny Client/Server'
  ClientHeight = 570
  ClientWidth = 1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 19
  object pnlEmployee: TPanel
    Left = 0
    Top = 0
    Width = 513
    Height = 570
    Align = alLeft
    Caption = 'pnlEmployee'
    TabOrder = 0
    object pnlEmployeeDetails: TPanel
      Left = 1
      Top = 440
      Width = 511
      Height = 129
      Align = alBottom
      TabOrder = 0
      object lblFirstName: TLabel
        Left = 24
        Top = 48
        Width = 71
        Height = 19
        Caption = 'FirstName'
      end
      object Label1: TLabel
        Left = 24
        Top = 81
        Width = 71
        Height = 19
        Caption = 'FirstName'
      end
      object navEmployee: TDBNavigator
        Left = 16
        Top = 6
        Width = 240
        Height = 25
        DataSource = dscEmployee
        TabOrder = 0
      end
      object edtFirstName: TDBEdit
        Left = 112
        Top = 48
        Width = 185
        Height = 27
        DataField = 'FIRSTNAME'
        DataSource = dscEmployee
        TabOrder = 1
      end
      object edtLastName: TDBEdit
        Left = 112
        Top = 81
        Width = 185
        Height = 27
        DataField = 'LASTNAME'
        DataSource = dscEmployee
        TabOrder = 2
      end
      object imgPhoto: TDBImage
        Left = 336
        Top = 48
        Width = 73
        Height = 65
        DataField = 'PHOTO'
        DataSource = dscEmployee
        PopupMenu = pmePhoto
        TabOrder = 3
      end
    end
    object grdEmployee: TDBGrid
      Left = 1
      Top = 42
      Width = 511
      Height = 398
      Align = alClient
      DataSource = dscEmployee
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -16
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
    object pnlEmployeeOptions: TPanel
      Left = 1
      Top = 1
      Width = 511
      Height = 41
      Align = alTop
      TabOrder = 2
      object Label2: TLabel
        Left = 24
        Top = 11
        Width = 93
        Height = 19
        Caption = 'Update Mode'
      end
      object cbxUpdateMode: TComboBox
        Left = 136
        Top = 8
        Width = 201
        Height = 27
        Style = csDropDownList
        ItemIndex = 1
        TabOrder = 0
        Text = 'Use Key Fields'
        OnChange = cbxUpdateModeChange
        Items.Strings = (
          'Use All Fields'
          'Use Key Fields'
          'Use Changed Fields'
          'Use Concurrency Mode')
      end
    end
  end
  object pnlLog: TPanel
    Left = 513
    Top = 0
    Width = 586
    Height = 570
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 1
    ExplicitWidth = 574
    object pnlLogHeader: TPanel
      Left = 1
      Top = 1
      Width = 584
      Height = 41
      Align = alTop
      Caption = 'SQL Log'
      TabOrder = 0
      ExplicitWidth = 572
    end
    object meoLog: TMemo
      Left = 1
      Top = 42
      Width = 584
      Height = 527
      Align = alClient
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 1
      WordWrap = False
    end
  end
  object dscEmployee: TDataSource
    DataSet = dmoMain.qryEmployee
    Left = 152
    Top = 200
  end
  object dlgSelectBitmap: TOpenDialog
    Filter = 'Bitmaps|*.bmp'
    Left = 441
    Top = 456
  end
  object pmePhoto: TPopupMenu
    Left = 441
    Top = 504
    object menChangePhoto: TMenuItem
      Caption = 'Change Photo'
      OnClick = menChangePhotoClick
    end
  end
end
