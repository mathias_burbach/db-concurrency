program DBCClient;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  DMain in 'DMain.pas' {dmoMain: TDataModule},
  UCommon in '..\Common\UCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
