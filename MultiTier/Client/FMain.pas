unit FMain;

interface

uses
  System.Classes,
  DataSnap.DBClient,
  Vcl.recerror,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Data.DB,
  Vcl.DBCtrls,
  Vcl.StdCtrls,
  Vcl.Mask,
  Vcl.Grids,
  Vcl.DBGrids,
  Vcl.ExtCtrls,
  Vcl.Menus;

type
  TfrmMain = class(TForm)
    pnlEmployee: TPanel;
    pnlEmployeeDetails: TPanel;
    grdEmployee: TDBGrid;
    pnlEmployeeOptions: TPanel;
    navEmployee: TDBNavigator;
    dscEmployee: TDataSource;
    edtFirstName: TDBEdit;
    lblFirstName: TLabel;
    Label1: TLabel;
    edtLastName: TDBEdit;
    imgPhoto: TDBImage;
    cbxUpdateMode: TComboBox;
    Label2: TLabel;
    dlgSelectBitmap: TOpenDialog;
    pmePhoto: TPopupMenu;
    menChangePhoto: TMenuItem;
    procedure cbxUpdateModeChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure menChangePhotoClick(Sender: TObject);
  private
    { Private declarations }
    procedure DoEmployeeReconcileError(DataSet: TCustomClientDataSet;
                                       E: EReconcileError;
                                       UpdateKind: TUpdateKind;
                                       var Action: TReconcileAction);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses
  System.SysUtils,
  DMain,
  UCommon;

procedure TfrmMain.cbxUpdateModeChange(Sender: TObject);
var
  UpdateMode: TSQLUpdateMode;
begin
  UpdateMode := TSQLUpdateMode(cbxUpdateMode.ItemIndex);
  dmoMain.SetUpdateMode(UpdateMode);
end;

procedure TfrmMain.DoEmployeeReconcileError(DataSet: TCustomClientDataSet;
                                            E: EReconcileError;
                                            UpdateKind: TUpdateKind;
                                            var Action: TReconcileAction);
begin
  Action := HandleReconcileError(DataSet, UpdateKind, E);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dmoMain := TdmoMain.Create(Self);
  dmoMain.cdsEmployee.OnReconcileError := DoEmployeeReconcileError;
  dmoMain.OpenEmployee;
  dscEmployee.DataSet := dmoMain.cdsEmployee;
end;

procedure TfrmMain.menChangePhotoClick(Sender: TObject);
begin
  if dlgSelectBitmap.Execute then
    dmoMain.LoadPhoto(dlgSelectBitmap.FileName);
end;

end.
