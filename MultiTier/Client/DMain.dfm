object dmoMain: TdmoMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 239
  Width = 455
  object conAppServer: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DBXDataSnap'
      'HostName=localhost'
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/'
      
        'DriverAssemblyLoader=Borland.Data.TDBXClientDriverLoader,Borland' +
        '.Data.DbxClientDriver,Version=24.0.0.0,Culture=neutral,PublicKey' +
        'Token=91d62ebb5b0d1b1b')
    Left = 104
    Top = 64
  end
  object cdsEmployee: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEmployee'
    RemoteServer = prcEmployee
    OnNewRecord = cdsEmployeeNewRecord
    BeforeApplyUpdates = cdsEmployeeBeforeApplyUpdates
    Left = 336
    Top = 64
    object cdsEmployeeEMPLOYEEID: TIntegerField
      DisplayLabel = 'ID'
      DisplayWidth = 5
      FieldName = 'EMPLOYEEID'
      Origin = 'EMPLOYEEID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsEmployeeFIRSTNAME: TStringField
      DisplayLabel = 'FirstName'
      DisplayWidth = 15
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Required = True
      Size = 25
    end
    object cdsEmployeeLASTNAME: TStringField
      DisplayLabel = 'LastName'
      DisplayWidth = 15
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
      Size = 25
    end
    object cdsEmployeePHOTO: TGraphicField
      FieldName = 'PHOTO'
      Origin = 'PHOTO'
      Visible = False
      BlobType = ftGraphic
    end
    object cdsEmployeeUPDNO: TIntegerField
      DisplayLabel = 'UpdNo'
      FieldName = 'UPDNO'
      Origin = 'UPDNO'
    end
  end
  object prcEmployee: TDSProviderConnection
    ServerClassName = 'TsvmEmployee'
    SQLConnection = conAppServer
    Left = 216
    Top = 64
  end
end
