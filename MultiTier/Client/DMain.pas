unit DMain;

interface

uses
  System.Classes,
  Data.DB,
  Data.SqlExpr,
  Data.DBXDataSnap,
  Data.DBXCommon,
  Data.FMTBcd,
  Datasnap.DBClient,
  Datasnap.DSConnect,
  IPPeerClient,
  UCommon;

type
  TdmoMain = class(TDataModule)
    conAppServer: TSQLConnection;
    cdsEmployee: TClientDataSet;
    prcEmployee: TDSProviderConnection;
    cdsEmployeeEMPLOYEEID: TIntegerField;
    cdsEmployeeFIRSTNAME: TStringField;
    cdsEmployeeLASTNAME: TStringField;
    cdsEmployeePHOTO: TGraphicField;
    cdsEmployeeUPDNO: TIntegerField;
    procedure cdsEmployeeNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsEmployeeBeforeApplyUpdates(Sender: TObject;
                                            var OwnerData: OleVariant);
  private
    { Private declarations }
    FPK: Integer;
    FUpdateMode: TSQLUpdateMode;
    function GetNextTempKey: Integer;
  public
    { Public declarations }
    procedure OpenEmployee;
    procedure SetUpdateMode(const UpdateMode: TSQLUpdateMode);
    procedure LoadPhoto(const FileName: string);
  end;

var
  dmoMain: TdmoMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}
{$R *.dfm}
{ TdmoMain }

procedure TdmoMain.cdsEmployeeBeforeApplyUpdates(Sender: TObject;
                                                 var OwnerData: OleVariant);
begin
  OwnerData := Ord(FUpdateMode);
end;

procedure TdmoMain.cdsEmployeeNewRecord(DataSet: TDataSet);
begin
  cdsEmployeeEMPLOYEEID.AsInteger := GetNextTempKey;
end;

procedure TdmoMain.DataModuleCreate(Sender: TObject);
begin
  FPK := 0;
  FUpdateMode := umWhereKey;
end;

function TdmoMain.GetNextTempKey: Integer;
begin
  Dec(FPK);
  Result := FPK;
end;

procedure TdmoMain.LoadPhoto(const FileName: string);
begin
  if cdsEmployee.State = dsBrowse then
    cdsEmployee.Edit;
  cdsEmployeePHOTO.LoadFromFile(FileName);
end;

procedure TdmoMain.OpenEmployee;
begin
  cdsEmployee.Open;
end;

procedure TdmoMain.SetUpdateMode(const UpdateMode: TSQLUpdateMode);
begin
  FUpdateMode := UpdateMode;
end;

end.
