program DBCServer;

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  FMain in 'FMain.pas' {frmMain},
  SEmployeeMethods in 'SEmployeeMethods.pas' {svmEmployee: TDSServerModule},
  UServerContainer in 'UServerContainer.pas' {scrEmployeeServer: TDataModule},
  UCommon in '..\Common\UCommon.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TscrEmployeeServer, scrEmployeeServer);
  Application.Run;
end.

