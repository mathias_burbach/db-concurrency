unit SEmployeeMethods;

interface

uses
  System.Classes,
  Data.DB,
  DataSnap.DSProviderDataModuleAdapter,
  DataSnap.DBClient,
  DataSnap.Provider,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.UI.Intf,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait,
  FireDAC.Stan.Param,
  FireDAC.DatS,
  FireDAC.DApt.Intf,
  FireDAC.DApt,
  FireDAC.Moni.Base,
  FireDAC.Moni.FlatFile,
  FireDAC.Comp.DataSet,
  FireDAC.Comp.Client,
  FireDAC.Moni.Custom,
  UCommon;

type
  TsvmEmployee = class(TDSServerModule)
    conEmployee: TFDConnection;
    qryEmployee: TFDQuery;
    qryEmployeeEMPLOYEEID: TIntegerField;
    qryEmployeeFIRSTNAME: TStringField;
    qryEmployeeLASTNAME: TStringField;
    qryEmployeePHOTO: TGraphicField;
    qryEmployeeUPDNO: TIntegerField;
    dspEmployee: TDataSetProvider;
    FDMoniCustomClientLink1: TFDMoniCustomClientLink;
    procedure dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                            SourceDS: TDataSet;
                                            DeltaDS: TCustomClientDataSet;
                                            UpdateKind: TUpdateKind;
                                            var Applied: Boolean);
    procedure FDMoniCustomClientLink1Output(ASender: TFDMoniClientLinkBase;
                                            const AClassName, AObjName, AMessage: string);
    procedure DSServerModuleCreate(Sender: TObject);
    procedure DSServerModuleDestroy(Sender: TObject);
    procedure dspEmployeeBeforeApplyUpdates(Sender: TObject;
                                            var OwnerData: OleVariant);
  private
    { Private declarations }
    FSQLUpdateMode: TSQLUpdateMode;
    function GetNextPK(const TableName: string): Integer;
    procedure ApplyUpdateMode(const DeltaDS: TCustomClientDataSet);
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses
  System.SysUtils,
  Datasnap.DSSession,
  Winapi.Windows;

procedure TsvmEmployee.ApplyUpdateMode(const DeltaDS: TCustomClientDataSet);
begin
  DeltaDS.FieldByName('UPDNO').ProviderFlags := [pfInUpdate, pfInWhere];
  case FSQLUpdateMode of
    umWhereAll: dspEmployee.UpdateMode := upWhereAll;
    umWhereKey: dspEmployee.UpdateMode := upWhereKeyOnly;
    umWhereChanged: dspEmployee.UpdateMode := upWhereChanged;
    umPKAndUpNo:
    begin
      dspEmployee.UpdateMode := upWhereKeyOnly;
      DeltaDS.FieldByName('UPDNO').ProviderFlags := [pfInUpdate, pfInWhere, pfInKey];
    end;
  end;
end;

procedure TsvmEmployee.dspEmployeeBeforeApplyUpdates(Sender: TObject;
                                                     var OwnerData: OleVariant);
begin
  FSQLUpdateMode := TSQLUpdateMode(Integer(OwnerData));
end;

procedure TsvmEmployee.dspEmployeeBeforeUpdateRecord(Sender: TObject;
                                                     SourceDS: TDataSet;
                                                     DeltaDS: TCustomClientDataSet;
                                                     UpdateKind: TUpdateKind;
                                                     var Applied: Boolean);
begin
  ApplyUpdateMode(DeltaDS);
  if UpdateKind = ukInsert then
    DeltaDS.FieldByName('EmployeeID').AsInteger := GetNextPK('Employee')
  else if UpdateKind = ukModify then
  begin
    if FSQLUpdateMode = umPKAndUpNo then
      DeltaDS.FieldByName('UPDNO').NewValue := DeltaDS.FieldByName('UPDNO').OldValue + 1;
  end;
end;

procedure TsvmEmployee.DSServerModuleCreate(Sender: TObject);
begin
  FDMoniCustomClientLink1.Tracing := True;
end;

procedure TsvmEmployee.DSServerModuleDestroy(Sender: TObject);
begin
  FDMoniCustomClientLink1.Tracing := False;
end;

procedure TsvmEmployee.FDMoniCustomClientLink1Output(ASender: TFDMoniClientLinkBase;
                                                     const AClassName, AObjName, AMessage: string);
begin
  OutputDebugString(PChar(AMessage));
end;

function TsvmEmployee.GetNextPK(const TableName: string): Integer;
begin
  Result := conEmployee.ExecSQLScalar(Format('Select Gen_ID(Seq%s, 1) From RDB$Database', [TableName]));
end;

end.
