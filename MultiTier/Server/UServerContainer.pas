unit UServerContainer;

interface

uses
  System.Classes,
  Datasnap.DSTCPServerTransport,
  Datasnap.DSServer,
  Datasnap.DSCommonServer,
  IPPeerServer,
  IPPeerAPI;

type
  TscrEmployeeServer = class(TDataModule)
    DSEmplooyeeServer: TDSServer;
    tcpServerTransport: TDSTCPServerTransport;
    sclEmployee: TDSServerClass;
    procedure sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                  var PersistentClass: TPersistentClass);
  private
    { Private declarations }
  public
  end;

var
  scrEmployeeServer: TscrEmployeeServer;

implementation

{$R *.dfm}

uses
  System.SysUtils,
  SEmployeeMethods,
  UCommon;

procedure TscrEmployeeServer.sclEmployeeGetClass(DSServerClass: TDSServerClass;
                                                 var PersistentClass: TPersistentClass);
begin
  PersistentClass := TsvmEmployee;
end;

end.
